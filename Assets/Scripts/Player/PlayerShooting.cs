﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{

    [SerializeField] Transform firePosition;
    [SerializeField] ShotEffectsManager shotsEffects;

    public float range = 100f;
    public int damagePerShot = 25;

    private int shootableMask;

    void Start () {
        shotsEffects.Initialize();                                              //Initialize() instantiates the prefab containing impact particle effect
	}


    private void Awake()
    {
        shootableMask = LayerMask.GetMask("Shootable");
    }


    void Update ()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            shotsEffects.PlayShotEffects();                                     //Play the fire effects on the gun

            RaycastHit hit;

            Ray ray = new Ray(firePosition.position, firePosition.forward);     //Create the straight ray from the fireposition to the forward direction
            bool result = Physics.Raycast(ray, out hit, range, shootableMask);  //If the created ray hits any shootable object 
                                                                                //then play the impact effect on that object

            if (result)
            {
                EnemyHealth enemyHealth = hit.collider.GetComponent<EnemyHealth>();
                shotsEffects.PlayImpactEffect(hit.point);

                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(damagePerShot, hit.point);          //decrease enemy health
                }

            }
        }
	}

}
