﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;                            // The amount of health the player starts the game with.
    public int currentHealth;                                   // The current health the player has.
    public Slider healthSlider;                                 // Reference to the UI's health bar.
    public Image damageImage;                                   // Reference to an image to flash on the screen on being hurt.
    public AudioClip deathClip;                                 // The audio clip to play when the player dies.
    public float flashSpeed = 5f;                               // The speed the damageImage will fade at.
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);     // The colour the damageImage is set to, to flash.


    //Animator anim;                                            // Reference to the Animator component.
    AudioSource playerAudio;                                    // Reference to the AudioSource component.
    //FirstPersonController playerController;  
    //PlayerMovement playerMovement;                            // Reference to the player's movement.
    PlayerShooting playerShooting;                              // Reference to the PlayerShooting script.
    bool isDead;                                                // Whether the player is dead.
    bool damaged;                                               // True when the player gets damaged.


    void Awake()
    {
        //anim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
        playerShooting = GetComponentInChildren<PlayerShooting>();

        currentHealth = startingHealth;
    }


    void Update()
    {
        if (damaged)                                            // If the player has just been damaged...
        {
            damageImage.color = flashColour;                    // set the colour of the damageImage to the flash colour.
        } else
        {
            // transition the colour back to clear.
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        damaged = false;                                        // Reset the damaged flag.
    }


    public void TakeDamage(int amount)
    {
        damaged = true;                                         // Set the damaged flag so the screen will flash.
        currentHealth -= amount;
        healthSlider.value = currentHealth;

        playerAudio.clip = deathClip;
        playerAudio.Play();

        if (currentHealth <= 0 && !isDead)                      // If the player has lost all it's health and the death flag hasn't been set yet...
        {
            Death();                                            // ... it should die.
        }
    }


    void Death()
    {
        isDead = true;                                          // Set the death flag so this function won't be called again.

        playerAudio.clip = deathClip;                           // Set the audiosource to play the death clip 
        playerAudio.Play();                                     //and play it(this will stop the hurt sound from playing).
        //playerMovement.enabled = false;                       // Turn off the movement and shooting scripts.
        playerShooting.enabled = false;

        //FindObjectOfType<GameManager>().EndGame();

    }
}
