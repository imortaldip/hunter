﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{
    public GameObject gameOver;


    public void EndGame()
    {
        //gameOver.GetComponent<Text>().enabled = true;
        gameOver.SetActive(true);

        Debug.Log("GAME OVER!!");
    }

}
