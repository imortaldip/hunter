﻿using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;            // The amount of health the enemy starts the game with.
    public int currentHealth;                   // The current health the enemy has.
    public int scoreValue = 10;                 // The amount added to the player's score when the enemy dies.
    public AudioClip deathClip;                 // The sound to play when the enemy dies.


    Animator anim;                              // Reference to the animator.
    AudioSource enemyAudio;                     // Reference to the audio source.
    ParticleSystem hitParticles;                // Reference to the particle system that plays when the enemy is damaged.
    CapsuleCollider capsuleCollider;            // Reference to the capsule collider.
    bool isDead;                                // Whether the enemy is dead.


    void Awake()
    {
        anim = GetComponent<Animator>();
        enemyAudio = GetComponent<AudioSource>();
        hitParticles = GetComponentInChildren<ParticleSystem>();
        capsuleCollider = GetComponent<CapsuleCollider>();

        currentHealth = startingHealth;
    }


    public void TakeDamage(int amount, Vector3 hitPoint)
    {
        if (isDead)                             // If the enemy is dead...
            return;                             // no need to take damage so exit the function.

        enemyAudio.Play();                      // Play the hurt sound effect.
        currentHealth -= amount;                // Reduce the current health by the amount of damage sustained.
        if (currentHealth <= 0)                 // If the current health is less than or equal to zero...

        {
            Death();                            // the enemy is dead.
        }
    }


    void Death()
    {
        isDead = true;                          // The enemy is dead.
        capsuleCollider.isTrigger = true;       // Turn the collider into a trigger so shots can pass through it.
        anim.SetTrigger("Dead");                // Tell the animator that the enemy is dead.

        enemyAudio.Play();                      // Change the audio clip of the audio source to the death clip and play it (this will stop the hurt clip playing).
        enemyAudio.clip = deathClip;
        Destroy(gameObject, 1f);
    }
}