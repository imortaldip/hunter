﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class AnimalsMovement : MonoBehaviour
{
    Transform player;                                // Reference to the player's position.
    NavMeshAgent nav;                                // Reference to the nav mesh agent.


    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        nav = GetComponent<NavMeshAgent>();
    }


    void Update()
    {
        nav.SetDestination(player.position);        // set the destination of the nav mesh agent to the player.
    }
}