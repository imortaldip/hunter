﻿using UnityEngine;
using System.Collections;


public class EnemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;     // The time in seconds between each attack.
    public int attackDamage = 5;               // The amount of health taken away per attack.


    Animator anim;                              // Reference to the animator component.
    GameObject player;                          // Reference to the player GameObject.
    PlayerHealth playerHealth;                  // Reference to the player's health.
    EnemyHealth enemyHealth;                    // Reference to this enemy's health.
    bool playerInRange;                         // Whether player is within the trigger collider and can be attacked.
    float timer;                                // Timer for counting up to the next attack.


    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");    // Setting up the references.
        playerHealth = player.GetComponent<PlayerHealth>();
        enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent<Animator>();
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)         // If the entering collider is the player, the player is in range.
        {
            playerInRange = true;
        }
    }


    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)         // If the exiting collider is the player, the player is no longer in range.
        {
            playerInRange = false;
        }
    }


    void Update()
    {
        timer += Time.deltaTime;                // Add the time since Update was last called to the timer.

        if (timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)  // If the timer exceeds the time between attacks
        {                                                                                   // the player is in range and this enemy is alive
            Attack();                           // attack.
        }

        if (playerHealth.currentHealth <= 0)    // If the player has zero or less health...
        {
            anim.SetTrigger("PlayerDead");      // tell the animator the player is dead.
        }
    }


    void Attack()
    {
        timer = 0f;                             // Reset the timer.

        if (playerHealth.currentHealth > 0)     // If the player has health to lose...
        {
            playerHealth.TakeDamage(attackDamage);  // damage the player.
        }
    }
}