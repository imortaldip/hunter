﻿using UnityEngine;

public class ShotEffectsManager : MonoBehaviour
{
    [SerializeField] ParticleSystem gunShots;
    [SerializeField] AudioSource gunAudio;
    [SerializeField] GameObject hitParticlePrefab;

    ParticleSystem hitParticleEffect;

    //Initialize() instantiates the prefab containing impact particle effect
    public void Initialize()
    {
        hitParticleEffect = Instantiate(hitParticlePrefab).GetComponent<ParticleSystem>();
    }

    //Play gun fire effect and audio in fireposition
    public void PlayShotEffects()
    {
        gunShots.Stop(true);
        gunShots.Play(true);
        gunAudio.Stop();
        gunAudio.Play();
    }

    //Play impact effect and target position of hit object
    public void PlayImpactEffect(Vector3 impactPosition)
    {
        hitParticleEffect.transform.position = impactPosition;
        hitParticleEffect.Stop();
        hitParticleEffect.Play();
    }
}